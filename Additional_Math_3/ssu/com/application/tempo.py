import matplotlib

matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import numpy
import math

import tkinter as tk
from tkinter import *
from tkinter import messagebox as mb

from typing import List


class ParseFunction:
    def __init__(self, function: str):
        self.fun_str = function.lower()
        self.x = int()

    # функция приведения к нужному виду
    def design_function(self, fun: str) -> str:
        # арифметические операции
        # отличаем унарный минус
        fun = fun.replace("(-", "(~")
        if fun[0] == "-":
            fun = "~" + fun[1:]
        # логические операции (для простоты работы обозначаем операции одним символом
        fun = fun.replace("xor", "r")
        fun = fun.replace("or", "o")
        fun = fun.replace("and", "a")
        fun = fun.replace("not", "n")
        fun = fun.replace(" ", "").lower()

        # расставляем скобки при унарном минусе
        index = fun.find("~")
        while index != -1:
            if fun[index + 1] != "(":
                op = re.search("\+|-|o|a|r", fun[index:]).start() + index
                fun = fun[:index + 1] + "(" + fun[index + 1: op] + ")" + fun[op:]
            index = fun.find("~", index + 1)

        # расставляем скобки при not
        index = fun.find("n")
        while index != -1:
            if fun[index + 1] != "(":
                op = re.search("/|\+|-|o|a|r|\*", fun[index:]).start() + index
                fun = fun[:index + 1] + "(" + fun[index + 1: op] + ")" + fun[op:]
            index = fun.find("n", index + 1)

        # вставка умножения
        fun = fun.replace("x(", "x*(")
        while fun.find("xx") != -1:
            fun = fun.replace("xx", "x*x")
        fun = fun.replace(")x", ")*x")
        fun = fun.replace("xn", "x*n")
        fun = fun.replace(")(", ")*(")

        i = 0
        arr_sym = ["x", "n", "("]
        while i < len(fun):
            if i + 1 < len(fun) and ((fun[i] == ")" or fun[i] == "x") and fun[i + 1].isdigit() or
                                     fun[i].isdigit() and (fun[i + 1] in arr_sym)):
                fun = fun[:i + 1] + "*" + fun[i + 1:]
            i += 1
        return fun

    # распарсим функцию
    def parse_function(self):
        # возможные операции: префиксные и инфиксные
        prefix_op = ["n", "~"]
        infix_op = {"+": 1, "-": 1, "*": 2, "/": 2, "^": 3, "o": 0, "a": 0, "r": 0}
        stack = []
        temp = self.fun_str

        temp = self.design_function(temp)

        # переводим поступившию на вход функцию в обратную польскую нотацию
        fun_out = []
        while temp:
            if temp[0] == "x":
                fun_out.append(temp[0])
                temp = temp[1:]
            elif temp[0].isdigit():
                d = ""
                while temp and temp[0].isdigit():
                    d += temp[0]
                    temp = temp[1:]
                fun_out.append(int(d))
            elif temp[0] in prefix_op or temp[0] == "(":
                stack.append(temp[0])
                temp = temp[1:]
            elif temp[0] == ")":
                if stack:
                    sym = stack.pop()
                    while sym != "(":
                        fun_out.append(sym)
                        if stack:
                            sym = stack.pop()
                        else:
                            raise Exception("Ошибка! Не хватает скобок!")
                else:
                    raise Exception("Ошибка! Не хватает скобок!")
                temp = temp[1:]
            elif temp[0] in infix_op:
                if stack:
                    sym = stack.pop()
                    while sym in prefix_op or \
                            sym in infix_op and infix_op[sym] > infix_op[temp[0]]:
                        fun_out.append(sym)
                        if stack:
                            sym = stack.pop()
                        else:
                            sym = ""
                            break
                    if sym:
                        stack.append(sym)
                stack.append(temp[0])
                temp = temp[1:]
            else:
                raise Exception("Ошибка! Неизвестный символ в функции!")
        # если в стеке остались НЕ операции, то ошибка
        lst = list(filter(lambda x: x not in prefix_op and x not in infix_op, stack))
        if lst:
            raise Exception("Ошибка! Не хватает скобок!")
        stack.reverse()
        fun_out.extend(stack)
        return fun_out


class FunctionFrame(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.grid()
        self.res = []
        self.create_form()

    # обработка кликов
    def clicked_general(self, param):
        fun = self.input_f()
        if fun is None: return
        p = self.input_p()
        if p is None: return
        k = self.input_k()
        if k is None: return
        parse_fun = ParseFunction(fun)
        # пытаемся распарсить поступившую на вход функцию
        try:
            result_parse = parse_fun.parse_function()
            if result_parse:
                try:
                    if param:
                        self.res = calculate_func(result_parse, p, k)[0]
                        self.draw()
                    else:
                        n = self.input_n()
                        if n is None: return
                        temp_res = calculate_func(result_parse, p, k)[1]
                        self.res = beta_projection(temp_res[0], temp_res[1], k, p, n)
                        self.draw()
                except Exception as e:
                    if e.__str__() != "":
                        raise e
                    raise Exception("Функция введена некорректно!")
            else:
                raise Exception("Функция пустая!")
        except Exception as e:
            mb.showerror("Ошибка парсинга", e.__str__())

    def clicked(self):
        self.clicked_general(True)

    def clicked_beta(self):
        self.clicked_general(False)

    def draw(self):
        self.f.clear()
        if self.res == []:
            return
        a = self.f.add_subplot(111)
        a.scatter(self.res[0], self.res[1], s=self.var.get())
        self.canv.draw()

    # методы для ввода параметров
    def input_f(self):
        f = self.txt_fun.get()
        if f:
            return f
        else:
            mb.showerror("Ошибка ввода функции", "Неверно введена функция!")

    def input_k(self):
        try:
            k = int(self.txt_k.get())
            if k >= 1:
                return k
            else:
                raise Exception
        except:
            mb.showerror("Ошибка ввода k", "k должно быть натуральным числом!")

    def input_p(self):
        try:
            p = int(self.txt_p.get())
            if p >= 2:
                return p
            else:
                raise Exception
        except:
            mb.showerror("Ошибка ввода p", "p должно быть натуральным числом >= 2!")

    def input_n(self):
        try:
            n = int(self.txt_n.get())
            if 2 <= n <= 100:
                return n
            else:
                raise Exception
        except:
            mb.showerror("Ошибка ввода n", "n должно быть натуральным числом из множества {2, 3, ..., 100}!")

    # создаём форму
    def create_form(self):
        lbl_fun = Label(self, text="Введите функцию f(x): ")
        lbl_fun.grid(column=0, row=0)
        self.txt_fun = Entry(self, width=58)
        self.txt_fun.grid(column=1, row=0, columnspan=3)

        lbl_p = Label(self, text="Введите p: ")
        lbl_p.grid(column=0, row=1)
        vcmd = (self.register(self.check_the_input_only_allows_digits_only), '%P')
        self.txt_p = Entry(self, width=18, validate="key", validatecommand=vcmd)
        self.txt_p.grid(column=1, row=1)

        lbl_k = Label(self, text="Введите k: ")
        lbl_k.grid(column=2, row=1)
        self.txt_k = Entry(self, width=18, validate="key", validatecommand=vcmd)
        self.txt_k.grid(column=3, row=1)

        lbl_n = Label(self, text="Введите n (2^(1/n)): ")
        lbl_n.grid(column=2, row=2)
        vcmd = (self.register(self.check_the_input_only_allows_digits_only), '%P')
        self.txt_n = Entry(self, width=18, validate="key", validatecommand=vcmd)
        self.txt_n.grid(column=3, row=2)

        btn = Button(self, text="Показать", command=self.clicked)
        btn.grid(column=0, row=4, columnspan=1)

        btn_beta = Button(self, text="Показать beta-проекцию", command=self.clicked_beta)
        btn_beta.grid(column=2, row=4, columnspan=2)

        lbl_s = Label(self, text="Размер точек: ")
        lbl_s.grid(column=0, row=3)

        self.var = DoubleVar()
        self.scale_size = Scale(self, from_=0.01, to=1, variable=self.var, command=self.on_scale, orient=tk.HORIZONTAL,
                                length=350, resolution=0.01)
        self.scale_size.grid(column=1, row=3, columnspan=3)

        self.f = Figure(figsize=(5, 5), dpi=100)
        self.canv = FigureCanvasTkAgg(self.f, self)
        self.canv.draw()
        self.canv.get_tk_widget().grid(column=0, row=5, columnspan=4)

    def on_scale(self, val):
        v = float(val)
        self.var.set(v)

    # валидатор, который позволяет вводить пользователю только цифры
    def check_the_input_only_allows_digits_only(self, inp):
        if inp.isdigit() or inp == "" or inp == "\b" or inp is None:
            return True
        else:
            return False


# TODO Функция для проверки деления
def check_div(num: int, p: int, module: int) -> int:
    if num % p != 0:
        return pow(num, module - 1, module)
    raise Exception("Некорректная функция")


# для каждой операции определяем её действие
def calculate_func(fun: list, p: int, k: int) -> ([List[int], List[int]], [List[int], List[int]]):
    module = int(pow(p, k))
    operation = {
        '+': lambda lst: lst + [(lst.pop() + lst.pop()) % module],
        '-': lambda lst: lst + [(module - lst.pop() + lst.pop()) % module],
        '*': lambda lst: lst + [(lst.pop() * lst.pop()) % module],
        '/': lambda lst: lst + [check_div(lst.pop(), p, module) * lst.pop()],
        '~': lambda lst: lst + [(module - lst.pop()) % module],
        '^': lambda lst: lst + [pow(lst.pop(-2), lst.pop(), module)],
        'a': lambda lst: lst + [lst.pop() & lst.pop()],
        'o': lambda lst: lst + [lst.pop() | lst.pop()],
        'n': lambda lst: lst + [~lst.pop()],
        'r': lambda lst: lst + [lst.pop() ^ lst.pop()],
    }
    res_x = []
    res_y = []

    # для всех х от 0 до p^k находим значение функции f mod p^k
    for x in range(0, module):
        stack = []
        for el in fun:
            if type(el) is int:
                stack.append(el)
            elif el == "x":
                stack.append(x)
            else:
                stack = operation[el](stack)
        res_x.append(x % module)
        res_y.append(stack[0] % module)
    return [numpy.array(res_x) / module, numpy.array(res_y) / module], [res_x, res_y]


# функция перевода в нужную нам систему счисления
def change_base(num: int, base: int) -> str:
    res = str(base) + ":"
    temp = []
    while num != 0:
        temp.append(str(num % base))
        num //= base
    temp.reverse()
    temp = "".join(temp)
    res += temp
    return res


# beta-проекция автоматного отображения
def beta_projection(x: List[int], y: List[int], k: int, p: int, n: int) -> [List[int], List[int]]:
    try:
        beta = pow(2, 1 / n)
        e_x, e_y = [], []
        beta_pow_k = pow(beta, k)
        for i in range(len(x)):
            # двоичные числа
            x_ss_p = change_base(x[i], p)[2:]
            y_ss_p = change_base(y[i], p)[2:]
            res_x_e, res_y_e = 0, 0
            beta_temp = 1
            for digit in x_ss_p:
                res_x_e += int(digit) * beta_temp
                beta_temp *= beta
            beta_temp = 1
            for digit in y_ss_p:
                res_y_e += int(digit) * beta_temp
                beta_temp *= beta
            res_x_e /= beta_pow_k
            res_y_e /= beta_pow_k
            e_x.append(res_x_e - math.floor(res_x_e))
            e_y.append(res_y_e - math.floor(res_y_e))
        return [e_x, e_y]
    except Exception as e:
        print(e.__str__())
        return [[], []]


if __name__ == "__main__":
    root = tk.Tk()
    root.title("Functions")
    hello_frame = FunctionFrame(root)
    hello_frame.mainloop()
