from typing import Final
import matplotlib

matplotlib.use("TkAgg")
from tkinter import *

ERROR: Final = "ERROR!"
ERROR_ANY: Final = "Operation cannot be done! Check function!"
ERROR_FUNC: Final = "Function has an incorrect amount of brackets!"


class Func:
    def __init__(self, func: str):
        self.func = func.lower()

    def set_brackets_uno_minus(self, fun, sym):
        ind = fun.find(sym)
        while ind != -1:
            if fun[ind + 1] != "(":
                operation = re.search("\+|-|W|Q|R", fun[ind:]).start() + ind
                fun = fun[:ind + 1] + "(" + fun[ind + 1: operation] + ")" + fun[operation:]
            ind = fun.find(sym, ind + 1)
        return fun

    def set_brackets_not(self, fun, sym):
        ind = fun.find(sym)
        while ind != -1:
            if fun[ind + 1] != "(":
                operation = re.search("/|\+|-|W|Q|R|\*", fun[ind:]).start() + ind
                fun = fun[:ind + 1] + "(" + fun[ind + 1: operation] + ")" + fun[operation:]
            ind = fun.find(sym, ind + 1)
        return fun

    def replace_sym(self, fun):
        func = fun.lower().replace("not", "E") \
            .replace(" ", "") \
            .replace("xor", "R") \
            .replace("or", "W") \
            .replace("and", "Q") \
            .replace("(-", "(~")
        func = "~" + func[1:] if func[0] == "-" else func
        func = self.set_brackets_uno_minus(func, "~")
        func = self.set_brackets_not(func, "E")
        func = func.replace("x(", "x*(")
        ind = func.find("xx")
        while ind != -1:
            func = func.replace("xx", "x*x")
            ind = func.find("xx")
        func = func.replace(")x", ")*x") \
            .replace("xn", "x*n") \
            .replace(")(", ")*(")
        k = 0
        available_elements = list("xn(")
        length = len(func)
        while k < length:
            if k + 1 < length \
                    and (((func[k] == ")" or func[k] == "x") and func[k + 1].isdigit())
                         or (func[k].isdigit() and (func[k + 1] in available_elements))):
                func = func[:k + 1] + "*" + func[k + 1:]
            k = k + 1
        return func

    def process_func(self):
        operation_group1 = {"+": 1, "-": 1, "*": 2, "/": 2, "^": 3, "R": 0, "Q": 0, "W": 0}
        operation_group2 = ["E", "~"]
        pol_order = []
        store = self.replace_sym(self.func)
        process_func = []
        while store:
            if store[0] == "x":
                process_func.append(store[0])
                store = store[1:]
            elif store[0] in operation_group2 or store[0] == "(":
                pol_order.append(store[0])
                store = store[1:]
            elif store[0] == ")":
                if pol_order:
                    elem = pol_order.pop()
                    while elem != "(":
                        process_func.append(elem)
                        if pol_order:
                            elem = pol_order.pop()
                        else:
                            raise Exception(ERROR + ERROR_FUNC)
                else:
                    raise Exception(ERROR + ERROR_FUNC)
                store = store[1:]
            elif store[0].isdigit():
                elem = ""
                while store and store[0].isdigit():
                    elem = elem + store[0]
                    store = store[1:]
                process_func.append(int(elem))
            elif store[0] in operation_group1:
                if pol_order:
                    elem = pol_order.pop()
                    while elem in operation_group2 or \
                            elem in operation_group1 \
                            and operation_group1[elem] > operation_group1[store[0]]:
                        process_func.append(elem)
                        if pol_order:
                            elem = pol_order.pop()
                        else:
                            elem = ""
                            break
                    if elem:
                        pol_order.append(elem)
                pol_order.append(store[0])
                store = store[1:]
            else:
                raise Exception(ERROR + ERROR_ANY)

        check_brackets = list(filter(lambda k: k not in operation_group2 and k not in operation_group1
                                     , pol_order))
        if check_brackets:
            raise Exception(ERROR + ERROR_FUNC)
        pol_order.reverse()
        process_func.extend(pol_order)
        return process_func
