from typing import Final
from typing import List

import numpy as np
import math
ERROR: Final = "ERROR!"
ERROR_FUNC: Final = "Function has an incorrect value!"


def check_div(num: int, p: int, module: int) -> int:
    if num % p != 0:
        return pow(num, module - 1, module)
    raise Exception(ERROR + ERROR_FUNC)


def process(func: list, p: int, k: int) -> ([List[int], List[int]], [List[int], List[int]]):
    modu = int(pow(p, k))
    operation = {
        '+': lambda lst: lst + [(lst.pop() + lst.pop()) % modu],
        '-': lambda lst: lst + [(modu - lst.pop() + lst.pop()) % modu],
        '*': lambda lst: lst + [(lst.pop() * lst.pop()) % modu],
        '/': lambda lst: lst + [check_div(lst.pop(), p, modu) * lst.pop()],
        '~': lambda lst: lst + [(modu - lst.pop()) % modu],
        '^': lambda lst: lst + [pow(lst.pop(-2), lst.pop(), modu)],
        'Q': lambda lst: lst + [lst.pop() & lst.pop()],
        'W': lambda lst: lst + [lst.pop() | lst.pop()],
        'E': lambda lst: lst + [~lst.pop()],
        'R': lambda lst: lst + [lst.pop() ^ lst.pop()],
    }
    x_values = []
    y_values = []

    for x in range(0, modu):
        store = []
        for elem in func:
            if type(elem) is int:
                store.append(elem)
            elif elem == "x":
                store.append(x)
            else:
                store = operation[elem](store)
        x_values.append(x % modu)
        y_values.append(store[0] % modu)
    return [np.array(x_values) / modu, np.array(y_values) / modu], [x_values, y_values]


def calculate_bpr(x: List[int], y: List[int], k: int, p: int, n: int) -> [List[int], List[int]]:
    try:
        beta = pow(2, 1 / n)
        x_values = []
        y_values = []
        beta_k = pow(beta, k)
        for i in range(len(x)):
            x_binary = int_to_binary(x[i], p)[2:]
            y_binary = int_to_binary(y[i], p)[2:]
            x_out = 0
            y_out = 0
            tempo = 1
            for elem in x_binary:
                x_out = x_out + int(elem) * tempo
                tempo = tempo * beta
            tempo = 1
            for elem in y_binary:
                y_out = y_out + int(elem) * tempo
                tempo = tempo * beta
            x_out = x_out / beta_k
            y_out = y_out / beta_k
            x_values.append(x_out - math.floor(x_out))
            y_values.append(y_out - math.floor(y_out))
        return [x_values, y_values]
    except Exception as e:
        print(e.__str__())
        return [[], []]


def int_to_binary(digit: int, base: int) -> str:
    new_digit = str(base) + ":"
    tempo = []
    while digit != 0:
        tempo.append(str(digit % base))
        digit //= base
    tempo.reverse()
    temp = "".join(tempo)
    new_digit += temp
    return new_digit
