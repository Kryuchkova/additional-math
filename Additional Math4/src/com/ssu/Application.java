package com.ssu;

import com.ssu.sec_van_der_corput.SeqVanDerCorput;

public final class Application {
    public static void main(String[] Args) {
        Application app = new Application();
        app.startApplication();
    }

    public void startApplication(){
        SeqVanDerCorput seqVanDerCorput = new SeqVanDerCorput(2, 0.0, 15);
        seqVanDerCorput.printSeqVanDerCorput();
        System.out.println();
        seqVanDerCorput.printMappingZ2(15);
    }
}
