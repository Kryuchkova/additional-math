package com.ssu.sec_van_der_corput;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SeqVanDerCorput {
    private int  b;
    private double x;
    private int amount;
    List<Double> seqVanDerCorput;


    public SeqVanDerCorput(int b, double x, int amount){
        this.b = b;
        this.x = x;
        this.amount = amount;
        seqVanDerCorput = calculate();
    }

    public void printSeqVanDerCorput() {
        System.out.println("Sequence van der Corput with Kakutani von Neumann mapping:");
        for (int i = 0; i < seqVanDerCorput.size(); i++) {
            System.out.print(seqVanDerCorput.get(i) + " ");
        }
    }

    public void printMappingZ2(int count) {
        List<Integer> seqZ2 = new ArrayList<>();
        IntStream.range(0, count + 1).forEach(x -> seqZ2.add(x));
        List<Double> monnMapping = new ArrayList<>();
        List<Integer> z2Mapping = new ArrayList<>();
        for (int i = 0; i < count + 1; i++) {
            Map<Double, List<Integer>> mapping = calculateMappingMonn(seqZ2.get(i));
            monnMapping.addAll(mapping.keySet());
            z2Mapping.addAll(mapping.values().stream().collect(Collectors.toList()).get(0));
        }

        System.out.println("Sequence van der Corput with Monn mapping:");
        Collections.reverse(monnMapping);
        System.out.print(monnMapping.toString().replaceAll("[\\[\\],]", "") + " ");
        System.out.println();
        System.out.println("Mapping Z2: ");
        Collections.reverse(z2Mapping);
        System.out.print(z2Mapping.toString().replaceAll("[\\[\\],]", "") + " ");
    }

    private int getParamK(double x, int b) {
        int k = 0;
        while (!(1 - 1 / Math.pow(b, k) <= x && x < 1 - 1 / Math.pow(b, k + 1))) {
            k = k + 1;
        }
        return k;
    }

    private List<Double> calculate() {
        seqVanDerCorput = new ArrayList<>();
        seqVanDerCorput.add(0.0);
        for (int i = 0; i < amount; i++) {
            int k = getParamK(x, b);
            x = calculateKakutanivonNeumann(k);
            seqVanDerCorput.add(x);
        }
        return seqVanDerCorput;
    }

    private double calculateKakutanivonNeumann(int k) {
        return x - 1 + (1 / Math.pow(b, k)) + (1 / Math.pow(b, k)) / b;
    }

    private Map<Double, List<Integer>> calculateMappingMonn(int y) {
        Map<Double, List<Integer>> mapping;
        if (y == 0) {
            mapping = new HashMap<Double, List<Integer>>() {
                {
                    put(0.0, new ArrayList<Integer>() {
                        {
                            add(0);
                        }
                    });
                }
            };
            return mapping;
        }
        else {
            double yVanCorpu = 0;
            double bk = 1.0 / b;
            List<Integer> seqBinary = new ArrayList<>();
            mapping = new HashMap<Double, List<Integer>>();
            while (y > 0) {
                int yi = y % b;
                seqBinary.add(yi);
                yVanCorpu = yVanCorpu + yi * bk;
                y = y / b;
                bk = bk / b;
            }
            mapping.put(yVanCorpu, seqBinary);
            return mapping;
        }
    }
}

