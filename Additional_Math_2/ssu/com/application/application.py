
import matplotlib

from ssu.com.application.graphics.graphics import Graphics

matplotlib.use("TkAgg")
import tkinter as tk

if __name__ == "__main__":
    root = tk.Tk()
    root.title("Additional Math - Projection")
    frame = Graphics(root)
    frame.mainloop()