
import tkinter as tk
from tkinter import *
from tkinter import messagebox

from typing import Final
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

from ssu.com.application.operation.operate import process

from ssu.com.application.func.func import Func

ERROR: Final = "ERROR!"
ERROR_OPER: Final = "Operation cannot be done!"
ERROR_FUNC: Final = "Function has an incorrect value!"
ERROR_P: Final = "Parameter P must be more or equal 2!"
ERROR_K: Final = "Parameter K must be natural number!"


class Graphics(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.grid()
        self.graph = []
        self.image()

    def func_value(self):
        f = self.func.get()
        if not f:
            messagebox.showerror(ERROR, ERROR_FUNC)
        else:
            return f

    def k_value(self):
        k = int(self.k_param.get())
        if k < 1:
            messagebox.showerror(ERROR, ERROR_K)
            return
        else:
            return k

    def p_value(self):
        p = int(self.p_param.get())
        if p < 2:
            messagebox.showerror(ERROR, ERROR_P)
            return
        else:
            return p

    def validate_graphics_parameters(self):
        if self.func_value() is None or self.p_value() is None or self.k_value() is None:
            return
        try:
            operate = Func(self.func_value())
            processing_func = operate.process_func()
            if not processing_func:
                raise Exception(ERROR + ERROR_FUNC)
            else:
                self.graph = process(processing_func, self.p_value(), self.k_value())[0]
                self.draw()
        except Exception as e:
            messagebox.showerror(ERROR + ERROR_OPER, e.__str__())

    def set_scaling(self, val):
        v = float(val)
        self.scale_val.set(v)

    def draw(self):
        self.result.clear()
        if not self.graph:
            return
        pl = self.result.add_subplot(111)
        pl.scatter(self.graph[0], self.graph[1], s=self.scale_val.get())
        self.canv.draw()

    def image(self):
        Label(self, text="Projection Builder", font="Helvetica 18").grid(row=0, column=0, sticky=W)
        Label(self, text="Enter F(X) = ", font="Helvetica 14").grid(row=1, column=0, sticky=W)
        self.func = Entry(self, width=100, font="Helvetica 14")
        self.func.grid(row=1, column=1, columnspan=4, sticky=W, padx=10, pady=10)

        validate_logger = (self.register(self.digital_validate), '%P')

        Label(self, text="Enter P = ", font="Helvetica 14").grid(row=2, column=0, sticky=W)
        self.p_param = Entry(self, width=25, validate="key", validatecommand=validate_logger, font="Helvetica 14")
        self.p_param.grid(row=2, column=1, sticky=W, padx=10, pady=10)

        Label(self, text="Enter K = ", font="Helvetica 14").grid(row=3, column=0, sticky=W)
        self.k_param = Entry(self, width=25, validate="key", validatecommand=validate_logger, font="Helvetica 14")
        self.k_param.grid(row=3, column=1, sticky=W, padx=10, pady=10)

        Label(self, text="Point Size: ", font="Helvetica 14").grid(row=4, column=0, sticky=W)
        self.scale_val = DoubleVar()
        self.scale_size = Scale(self, from_=0.01, to=1, variable=self.scale_val, command=self.set_scaling,
                                orient=tk.HORIZONTAL,
                                length=1000, resolution=0.02, font="Helvetica 14", troughcolor='white')
        self.scale_size.grid(row=4, column=1, columnspan=4, sticky=W)

        btn = Button(self, text="BUILD", command=self.validate_graphics_parameters, height=2, width=20, bg='white',
                     highlightthickness=4)
        btn.grid(row=5, column=0, sticky=W, columnspan=1, padx=10, pady=10)

        self.result = Figure(figsize=(5, 5), dpi=100)
        self.canv = FigureCanvasTkAgg(self.result, self)
        self.canv.draw()
        self.canv.get_tk_widget().grid(row=7, column=1, columnspan=3, sticky=W, padx=10, pady=10)

    def digital_validate(self, value):
        if value.isdigit() or value.isspace() \
                or value == "" or value is None:
            return True
        else:
            return False
