from typing import Final
from typing import List

import numpy as np

ERROR: Final = "ERROR!"
ERROR_FUNC: Final = "Function has an incorrect value!"


def check_div(num: int, p: int, module: int) -> int:
    if num % p != 0:
        return pow(num, module - 1, module)
    raise Exception(ERROR + ERROR_FUNC)


def process(func: list, p: int, k: int) -> ([List[int], List[int]], [List[int], List[int]]):
    modu = int(pow(p, k))
    operation = {
        '+': lambda lst: lst + [(lst.pop() + lst.pop()) % modu],
        '-': lambda lst: lst + [(modu - lst.pop() + lst.pop()) % modu],
        '*': lambda lst: lst + [(lst.pop() * lst.pop()) % modu],
        '/': lambda lst: lst + [check_div(lst.pop(), p, modu) * lst.pop()],
        '~': lambda lst: lst + [(modu - lst.pop()) % modu],
        '^': lambda lst: lst + [pow(lst.pop(-2), lst.pop(), modu)],
        'Q': lambda lst: lst + [lst.pop() & lst.pop()],
        'W': lambda lst: lst + [lst.pop() | lst.pop()],
        'E': lambda lst: lst + [~lst.pop()],
        'R': lambda lst: lst + [lst.pop() ^ lst.pop()],
    }
    x_values = []
    y_values = []

    for x in range(0, modu):
        store = []
        for elem in func:
            if type(elem) is int:
                store.append(elem)
            elif elem == "x":
                store.append(x)
            else:
                store = operation[elem](store)
        x_values.append(x % modu)
        y_values.append(store[0] % modu)
    return [np.array(x_values) / modu, np.array(y_values) / modu], [x_values, y_values]

