package com.ssu;

import com.ssu.polynomial.Polynomial;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public final class Application {
    public static void main(String[] Args) {
        Application app = new Application();
        app.startApplication();
    }

    public void startApplication(){
        Polynomial polynomial = createPoly();
        checkTransitivity(polynomial);
        checkBijectivity(polynomial);
    }

    public void checkTransitivity(Polynomial polynomial) {
        if (polynomial.isTransitive()) {
            System.out.println("Polynomial F= " + polynomial.toString() + " is transitive");
        } else {
            System.out.println("Polynomial F= " + polynomial.toString() + " isn't transitive");
        }
    }

    public void checkBijectivity(Polynomial polynomial) {
        if (polynomial.isBijectivitive()) {
            System.out.println("Polynomial F= " + polynomial.toString() + " is bijectivitive");
        } else {
            System.out.println("Polynomial F= " + polynomial.toString() + " isn't bijectivitive");
        }
    }

    public Polynomial createPoly() {
        Scanner in = new Scanner(System.in);
        System.out.print("Please, enter polynomial degree: ");
        int polynomialDegree = in.nextInt();
        List<Integer> coefficients = enterCoefficients(polynomialDegree);
        return new Polynomial(polynomialDegree,coefficients);
    }

    public List<Integer> enterCoefficients(int polynomialDegree) {
        Scanner in = new Scanner(System.in);
        List<Integer> coefficients = new ArrayList<>();
        for (int i = polynomialDegree; i >= 0 ; i--) {
            System.out.println("Please, enter a coefficient for x^" + i + ":");
            coefficients.add(in.nextInt());
        }
        return coefficients;
    }
}
