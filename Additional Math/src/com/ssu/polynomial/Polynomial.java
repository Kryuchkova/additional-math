package com.ssu.polynomial;

import java.util.*;

public class Polynomial implements Transivitive, Bijectivitive{
    private static final int DEFAULT_MOD_TRANSITIVITY = 8;
    private static final int DEFAULT_MOD_BIJECTIVITY = 4;

    private List<Integer> coefficients;
    private Map<String, Integer> polynomial;
    private int polynomialDegree;


    public Polynomial(int polynomialDegree, List<Integer> coefficients){
        this.polynomialDegree = polynomialDegree;
        this.coefficients = coefficients;
        polynomial = new HashMap<>();
        for (int i = coefficients.size() - 1, j = 0; i >= 0 && j <= coefficients.size(); i--, j++) {
            polynomial.put("x^" + i, coefficients.get(j));
        }
    }

    @Override
    public boolean isTransitive(){
        boolean isTransitive = false;
        List<Integer> residue = new ArrayList<>();
        residue.add(0);
        int i = 0;
        while (!isTransitive) {
            int newVal = calculate(residue.get(i)) % DEFAULT_MOD_TRANSITIVITY;
            residue.add(newVal);
            Set<Integer> residues = new HashSet<>(residue);
            if (newVal == residue.get(0) && residues.size() == DEFAULT_MOD_TRANSITIVITY) {
                isTransitive = true;
            }
            i++;
            if (!isTransitive && i > DEFAULT_MOD_TRANSITIVITY) {
                break;
            }
        }
        return isTransitive;
    }


    @Override
    public boolean isBijectivitive() {
        Map<Integer, Integer> residue = new HashMap<>();
        for (int i = 0; i < DEFAULT_MOD_BIJECTIVITY; i++) {
            residue.put(i, calculate(i) % DEFAULT_MOD_BIJECTIVITY);
        }
        Set<Integer> residues = new HashSet<>(residue.values());
        return residues.size() == DEFAULT_MOD_BIJECTIVITY;
    }

    public Map<String, Integer> getPolynomial() {
        return polynomial;
    }

    public int getPolynomialDegree() {
        return polynomialDegree;
    }

    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        for (Map.Entry<String, Integer> item : polynomial.entrySet()) {
            if (item.getValue() != 1 || item.getValue() == 0) {
                str.append(item.getValue());
            }
            if (item.getValue() != 0) {
                str.append(item.getKey());
            } else {
                str.append(item.getValue());
            }
            str.append(" + ");
        }
        str.deleteCharAt(str.lastIndexOf("+"));
        return str.toString();
    }

    private int calculate(Integer xValue) {
        int result = 0;
        for (int i = 0, j = coefficients.size() - 1; i < coefficients.size() && j >= 0; i++, j--) {
            result += coefficients.get(j) * Math.pow(xValue, i);
        }
        return result;
    }


}

