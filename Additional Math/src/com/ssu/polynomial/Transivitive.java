package com.ssu.polynomial;

public interface Transivitive {
    boolean isTransitive();
}
