package com.ssu.hash_func;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.ArrayList;
import java.util.List;

public final class Application {
    public static void main(String[] Args) {
        Application app = new Application();
        app.startApplication();
    }

    public void startApplication() {
        String f = "((x^2 & 4) + (x^3 | 1) + (x << 3))";
        int m = 9;
        String w = "1111001100011010010110";
        String h0 = "100111101";
        System.out.println(calculateHashValue(f, m, w, h0));
    }


    private String convertFromBaseToBase(String str, int fromBase, int toBase) {
        return Integer.toString(Integer.parseInt(str, fromBase), toBase);
    }

    private String getHashFuncValue(String func, String x, String y, int module) {
        x = Integer.toString(Integer.parseInt(x, 2));
        y = Integer.toString(Integer.parseInt(y, 2));
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");
        try {
            String script1 = "x = " + x;
            String script2 = "y = " + y;
            engine.eval(script1);
            engine.eval(script2);
            long result = Math.round((Double)engine.eval(func));
            Object hash = result % module;
            //System.out.printf("x: %s; y: %s; res: %s; hash: %s;\n", x, y, result, convertFromBaseToBase(hash.toString(), 10, 2));
            return convertFromBaseToBase(hash.toString(), 10, 2);
        }
        catch (Exception ex) {
            throw new ArithmeticException("\nERROR! Check function and try again");
        }
    }

    private String addZeros(String str, int unitSize) {
        if (str.length() % unitSize != 0) {
            int countZeros = unitSize - str.length() % unitSize;
            String temp = new StringBuilder(str).reverse().toString();
            for (int i = 0; i < countZeros; i++) {
                temp = new StringBuilder(temp).append('0').toString();
            }
            str = new StringBuilder(temp).reverse().toString();
        }
        return str;
    }

    private List<String> splitBinaryStringIntoUnits(String binaryString, int unitSize) {
        binaryString = addZeros(binaryString, unitSize);
        List<String> result = new ArrayList<>();
        for (int i = binaryString.length(); i > 0; i -= unitSize) {
            result.add(binaryString.substring(i - unitSize, i));
        }
        return result;
    }

    public String calculateHashValue(String func, int unitSize, String binaryString, String h0) {
        int unitbase = 2 << unitSize;
        List<String> partOfBinaryString = splitBinaryStringIntoUnits(binaryString, unitSize);
        int countUnits = binaryString.length() / unitSize;
        List<String> h = new ArrayList<>();
        h.add(h0);

        for (int i = 0; i < countUnits; i++) {
            h.add(getHashFuncValue(func, partOfBinaryString.get(i), h.get(i), unitbase));
        }

        return addZeros(h.get(h.size() - 1), unitSize);
    }
}
